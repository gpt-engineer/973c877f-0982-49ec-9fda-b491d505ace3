let stopwatchInterval;
let timerInterval;
let stopwatchTime = 0;
let timerTime = 0;

document
  .getElementById("startStopwatch")
  .addEventListener("click", function () {
    if (!timerInterval) {
      stopwatchInterval = setInterval(function () {
        stopwatchTime++;
        document.getElementById("stopwatch").innerText = new Date(
          stopwatchTime * 1000,
        )
          .toISOString()
          .substr(11, 8);
      }, 1000);
    }
  });

document.getElementById("stopStopwatch").addEventListener("click", function () {
  clearInterval(stopwatchInterval);
});

document
  .getElementById("resetStopwatch")
  .addEventListener("click", function () {
    clearInterval(stopwatchInterval);
    stopwatchTime = 0;
    document.getElementById("stopwatch").innerText = "00:00:00";
  });

document.getElementById("startTimer").addEventListener("click", function () {
  if (!stopwatchInterval) {
    let hours = parseInt(document.getElementById("hours").value);
    let minutes = parseInt(document.getElementById("minutes").value);
    let seconds = parseInt(document.getElementById("seconds").value);
    timerTime = hours * 3600 + minutes * 60 + seconds;
    timerInterval = setInterval(function () {
      if (timerTime <= 0) {
        clearInterval(timerInterval);
      } else {
        timerTime--;
        document.getElementById("timer").innerText = new Date(timerTime * 1000)
          .toISOString()
          .substr(11, 8);
      }
    }, 1000);
  }
});

document.getElementById("stopTimer").addEventListener("click", function () {
  clearInterval(timerInterval);
});

document.getElementById("resetTimer").addEventListener("click", function () {
  clearInterval(timerInterval);
  timerTime = 0;
  document.getElementById("timer").innerText = "00:00:00";
});
